use crate::lines_codec::LinesCodec;

use tokio::{
    prelude::*,
    codec, net as anet,
};

mod link;
pub use link::Link;

mod state;
pub use state::State;



pub struct User<F> {
    forward: F,
}

pub fn user(socket: anet::TcpStream, link: Link)
-> User<impl Future<Item=(), Error=()>> {
    let (socket_writer, socket_reader) = {
        let codec = LinesCodec::new();
        let framed = codec::Framed::new(socket, codec);
        let (writer, reader) = framed.split();

        let writer = writer.sink_map_err(|e| eprintln!("failed to write to socket, {}", e));
        let reader = reader.map_err(|e| eprintln!("failed to read from socket, {}", e));
        
        (writer, reader)
    };

    let (link_writer, link_reader) = {
        let (writer, reader) = link.split();

        let writer = writer.sink_map_err(|e| eprintln!("failed to write to link, {}", e));
        let reader = reader.map_err(|e| eprintln!("failed to reade from link, {}", e));

        (writer, reader)
    };

    let forward = {
        let forward_in = socket_reader.forward(link_writer);
        let forward_out = link_reader.forward(socket_writer);

        forward_in
            .join(forward_out)
            .map(|_| ())
    };

    User {
       forward,
    }
}

impl<F> Future for User<F>
where F: Future<Item=(), Error=()> {
    type Item = ();
    type Error = ();

    fn poll(&mut self) -> Poll<Self::Item, Self::Error> {
        self.forward.poll()
    }
}
