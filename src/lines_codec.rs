use tokio::codec::{ Decoder, Encoder };

use bytes::{ BufMut, BytesMut };

use std::io;

mod bytes_utf8_view;
pub use bytes_utf8_view::BytesUtf8View;

mod error;
pub use error::LinesCodecError;
use error::MaxLenExdedError;

mod utility;
use utility::{ without_carriage, shorten };



#[derive(Clone, Debug, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct LinesCodec {
    passed: usize,
    max_length: Option<usize>,
}

impl LinesCodec {
    pub fn new() -> Self {
        Self {
            passed: 0,
            max_length: None,
        }
    }

    pub fn with_max_length(max_length: usize) -> Self {
        assert!(max_length > 0, "length must be greater than 0");
        Self {
            max_length: Some(max_length),
            ..Self::new()
        }
    }

    pub fn max_length(&self) -> Option<usize> {
        self.max_length
    }
}

impl Decoder for LinesCodec {
    type Item = BytesUtf8View;
    type Error = LinesCodecError;

    fn decode(&mut self, src: &mut BytesMut) -> Result<Option<Self::Item>, Self::Error> {
        let pos = src[self.passed..]
            .iter()
            .position(|e| *e == b'\n');

        if let Some(n) = pos {
            self.passed = 0;
            let mut cut = src.split_to(n + 1);

            if let Some(max_length) = self.max_length {
                if n > max_length {
                    let error = MaxLenExdedError::try_new(cut, max_length)?;
                    return Err(LinesCodecError::MaxLenExded(error))
                }
            }
            
            shorten(&mut cut, 1);
            without_carriage(&mut cut);
            let bytes_utf8_view = BytesUtf8View::from_utf8(cut)?;

            Ok(Some(bytes_utf8_view))
        } else {
            if let Some(max_length) = self.max_length {
                if src.len() > max_length {
                    self.passed = 0;

                    let error = MaxLenExdedError::try_new(src.take(), max_length)?;
                    return Err(LinesCodecError::MaxLenExded(error))
                }
            }

            self.passed = src.len();
            Ok(None)
        }
    }

    fn decode_eof(&mut self, buf: &mut BytesMut) -> Result<Option<Self::Item>, Self::Error> {
        match self.decode(buf)? {
            Some(frame) => Ok(Some(frame)),
            None => {
                // No terminating newline - return remaining data, if any
                if buf.is_empty() || buf == &b"\r"[..] {
                    Ok(None)
                } else {
                    self.passed = 0;

                    let mut bytes = buf.take();
                    without_carriage(&mut bytes);
                    let bytes_utf8_view = BytesUtf8View::from_utf8(bytes)?;

                    Ok(Some(bytes_utf8_view))
                }
            }
        }
    }
}

impl Encoder for LinesCodec {
    type Item = BytesUtf8View;
    type Error = io::Error;

    fn encode(&mut self, item: Self::Item, dst: &mut BytesMut) -> Result<(), Self::Error> {
        dst.reserve(item.len() + 1);
        dst.put(item.as_str());
        dst.put_u8(b'\n');

        Ok(())
    }
}
