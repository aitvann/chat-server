use bytes::{ BytesMut, Bytes };

use std::{ io, str, fmt };

use super::{
    bytes_utf8_view::BytesUtf8View,
    utility::without_carriage,
};


#[derive(Debug)]
pub struct FromUtf8Error {
    bytes: BytesMut,
    error: str::Utf8Error,
}

impl FromUtf8Error {
    pub fn new(bytes: BytesMut, error: str::Utf8Error) -> Self {
        Self { bytes, error }
    }

    pub fn as_bytes(&self) -> &BytesMut {
        &self.bytes
    }

    pub fn into_bytes(self) -> BytesMut {
        self.bytes
    }

    pub fn utf8_error(&self) -> str::Utf8Error {
        self.error
    }
}

impl fmt::Display for FromUtf8Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "invalid utf8 sequance")
    }
}


#[derive(Debug)]
pub struct MaxLenExdedError {
    bytes_utf8_view: BytesUtf8View,
    discarded_length: usize,
}

impl MaxLenExdedError {
    // TODO: rename to "from"
    pub fn try_new(mut src: BytesMut, max_length: usize) -> Result<Self, MaxLenExdedAndFromUtf8Error> {
        assert!(src.len() > max_length, "length of source is too large");

        let discarded_length = src.len() - max_length;
        let mut cut = src.split_to(max_length);
        without_carriage(&mut cut);

        let bytes_utf8_view = BytesUtf8View::from_utf8(src)
            .map_err(|err| {
                let utf8_error = err.utf8_error();
                let bytes = err.into_bytes();

                MaxLenExdedAndFromUtf8Error::new(bytes, utf8_error, discarded_length)
            })?;

        let error = Self { bytes_utf8_view, discarded_length };
        Ok(error)
    }

    pub fn new(src: BytesMut, max_length: usize) -> Self {
        Self::try_new(src, max_length)
            .unwrap_or_else(|_| panic!("invalid utf8"))
    }

    pub fn as_bytes_utf8_view(&self) -> &BytesUtf8View {
        &self.bytes_utf8_view
    }

    pub fn into_bytes_utf8_view(self) -> BytesUtf8View {
        self.bytes_utf8_view
    }

    pub fn discarded_length(&self) -> usize {
        self.discarded_length
    }
}

impl fmt::Display for MaxLenExdedError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "maximum line length exceeded")
    }
}


#[derive(Debug)]
pub struct MaxLenExdedAndFromUtf8Error {
    bytes: BytesMut,
    error: str::Utf8Error,
    discarded_length: usize,
}

impl MaxLenExdedAndFromUtf8Error {
    fn new(bytes: BytesMut, error: str::Utf8Error, discarded_length: usize) -> Self {
        Self { bytes, error, discarded_length }
    }

    pub fn as_bytes(&self) -> &BytesMut {
        &self.bytes
    }

    pub fn into_bytes(self) -> BytesMut {
        self.bytes
    }
    
    pub fn utf8_error(&self) -> str::Utf8Error {
        self.error
    }

    pub fn discarded_length(&self) -> usize {
        self.discarded_length
    }
}

impl fmt::Display for MaxLenExdedAndFromUtf8Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "maximum line length exceeded and invalid utf8 string")
    }
}



#[derive(Debug)]
pub enum LinesCodecError {
    MaxLenExdedAndFromUtf8(MaxLenExdedAndFromUtf8Error),
    MaxLenExded(MaxLenExdedError),
    FromUtf8(FromUtf8Error),
    IO(io::Error),
}

impl From<MaxLenExdedAndFromUtf8Error> for LinesCodecError {
    fn from(err: MaxLenExdedAndFromUtf8Error) -> Self {
        LinesCodecError::MaxLenExdedAndFromUtf8(err)
    }
}

impl From<MaxLenExdedError> for LinesCodecError {
    fn from(err: MaxLenExdedError) -> Self {
        LinesCodecError::MaxLenExded(err)
    }
}

impl From<FromUtf8Error> for LinesCodecError {
    fn from(err: FromUtf8Error) -> Self {
        LinesCodecError::FromUtf8(err)
    }
}

impl From<io::Error> for LinesCodecError {
    fn from(err: io::Error) -> Self {
        LinesCodecError::IO(err)
    }
}

impl fmt::Display for LinesCodecError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::MaxLenExdedAndFromUtf8(err) => err.fmt(f),
            Self::MaxLenExded(err) => err.fmt(f),
            Self::FromUtf8(err) => err.fmt(f),
            Self::IO(err) => err.fmt(f),
        }
    }
}
