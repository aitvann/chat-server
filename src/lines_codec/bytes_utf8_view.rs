use bytes::BytesMut;

use super::{ utility, error::FromUtf8Error };

use std::{
    str,
    ops::{ self, Deref, DerefMut },
};



#[derive(Clone, Debug, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct BytesUtf8View {
    bytes: BytesMut,
}

impl BytesUtf8View {
    pub fn from_utf8(bytes: BytesMut) -> Result<Self, FromUtf8Error> {
        match utility::is_valid_utf8(&bytes) {
            Ok(_) => {
                let utf8_bytes = Self { bytes };
                Ok(utf8_bytes)
            },
            Err(error) => Err(FromUtf8Error::new(bytes, error))
        }
    }

    pub unsafe fn from_utf8_unchecked(bytes: BytesMut) -> Self {
        Self { bytes }
    }

    pub fn as_bytes(&self) -> &BytesMut {
        &self.bytes
    }

    pub fn into_bytes(self) -> BytesMut {
        self.bytes
    }

    pub fn as_mut_str(&mut self) -> &mut str {
        self
    }

    pub fn as_str(&self) -> &str {
        self
    }

    pub fn is_empty(&self) -> bool {
        self.bytes.is_empty()
    }

    pub fn len(&self) -> usize {
        self.bytes.len()
    } 
}

impl Deref for BytesUtf8View {
    type Target = str;

    fn deref(&self) -> &Self::Target {
        unsafe { str::from_utf8_unchecked(&self.bytes) }
    }
}

impl DerefMut for BytesUtf8View {
    fn deref_mut(&mut self) -> &mut Self::Target {
        unsafe { str::from_utf8_unchecked_mut(&mut self.bytes) }
    }
}

impl ops::Index<ops::Range<usize>> for BytesUtf8View {
    type Output = str;

    fn index(&self, index: ops::Range<usize>) -> &str {
        &self[..][index]
    }
}

impl ops::Index<ops::RangeTo<usize>> for BytesUtf8View {
    type Output = str;

    fn index(&self, index: ops::RangeTo<usize>) -> &str {
        &self[..][index]
    }
}

impl ops::Index<ops::RangeFrom<usize>> for BytesUtf8View {
    type Output = str;

    fn index(&self, index: ops::RangeFrom<usize>) -> &str {
        &self[..][index]
    }
}

impl ops::Index<ops::RangeFull> for BytesUtf8View {
    type Output = str;

    fn index(&self, _index: ops::RangeFull) -> &str {
        self
    }
}

impl ops::Index<ops::RangeInclusive<usize>> for BytesUtf8View {
    type Output = str;

    fn index(&self, index: ops::RangeInclusive<usize>) -> &str {
        ops::Index::index(&**self, index)
    }
}

impl ops::Index<ops::RangeToInclusive<usize>> for BytesUtf8View {
    type Output = str;

    fn index(&self, index: ops::RangeToInclusive<usize>) -> &str {
        ops::Index::index(&**self, index)
    }
}

impl ops::IndexMut<ops::Range<usize>> for BytesUtf8View {
    fn index_mut(&mut self, index: ops::Range<usize>) -> &mut str {
        &mut self[..][index]
    }
}

impl ops::IndexMut<ops::RangeTo<usize>> for BytesUtf8View {
    fn index_mut(&mut self, index: ops::RangeTo<usize>) -> &mut str {
        &mut self[..][index]
    }
}

impl ops::IndexMut<ops::RangeFrom<usize>> for BytesUtf8View {
    fn index_mut(&mut self, index: ops::RangeFrom<usize>) -> &mut str {
        &mut self[..][index]
    }
}

impl ops::IndexMut<ops::RangeFull> for BytesUtf8View {
    fn index_mut(&mut self, _index: ops::RangeFull) -> &mut str {
        self
    }
}

impl ops::IndexMut<ops::RangeInclusive<usize>> for BytesUtf8View {
    fn index_mut(&mut self, index: ops::RangeInclusive<usize>) -> &mut str {
        ops::IndexMut::index_mut(&mut **self, index)
    }
}

impl ops::IndexMut<ops::RangeToInclusive<usize>> for BytesUtf8View {
    fn index_mut(&mut self, index: ops::RangeToInclusive<usize>) -> &mut str {
        ops::IndexMut::index_mut(&mut **self, index)
    }
}
