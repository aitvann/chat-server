use bytes::BytesMut;

use std::str;



// pub fn utf8(bytes: BytesMut) -> Result<String, string::FromUtf8Error> {
//     let vec = Vec::from(&*bytes);
//     let string = String::from_utf8(vec)?;

//     Ok(string)
// }

pub fn without_carriage(bytes: &mut BytesMut) {
    if bytes.ends_with(&b"\r"[..]) {
        shorten(bytes, 1);
    }
}

pub fn shorten(bytes: &mut BytesMut, cnt: usize) {
    assert!(cnt <= bytes.len(), "can't be shortened more than the length of the buffer");
    bytes.truncate(bytes.len() - cnt);
}

pub fn is_valid_utf8(bytes: &[u8]) -> Result<(), str::Utf8Error> {
    str::from_utf8(bytes).map(|_| ())
}
