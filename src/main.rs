use tokio::{
    net as anet,
    prelude::{Future, Stream},
};

mod lines_codec;

mod user;
use user::{user, State};



fn main() {
    let matches = clap::App::new("chat server")
        .version("0.1.0")
        .author("Ivan A. <IVAN38562@gmail.com>")
        .about("processes multiple clients by distributing messages")
        .arg(clap::Arg::with_name("addresses")
            .short("a")
            .multiple(true)
            .takes_value(true)
            .help("address of server")
            .default_value("0.0.0.0:4732"))
        .get_matches();

    let listener = matches
        .values_of("addresses")
        .unwrap()
        .filter_map(|s| s.parse().ok())
        .filter_map(|addr| anet::TcpListener::bind(&addr).ok())
        .next()
        .expect("failed to bind to addresses");

    let state = State::new();

    let server = listener
        .incoming()
        .map_err(|e| eprintln!("failed to accept client: {}", e))
        .for_each(move |socket| {
            let addr = socket.peer_addr()
                .expect("failed to ger peer address");

            println!("client accepted: {}", addr);

            let state = state.clone();
            let user = state.create_link(addr)
                .and_then(|link| user(socket, link));

            tokio::spawn(user)
        });

    tokio::run(server);
}
