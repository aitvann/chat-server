use super::state::{ State, ChannelReciever };

use crate::lines_codec::BytesUtf8View;

use tokio::{prelude::*, sync::mpsc};

use futures::StartSend;

use std::net::SocketAddr;



pub struct Link {
    addr: SocketAddr,
    state: State,
    channel_reciever: ChannelReciever,
}

impl Link {
    pub(super) fn new(
        addr: SocketAddr,
        state: State,
        channel_reciever: ChannelReciever)
    -> Self {
        Self { addr, state, channel_reciever }
    }
}

impl Stream for Link {
    type Item = BytesUtf8View;
    type Error = mpsc::error::RecvError;

    fn poll(&mut self) -> Poll<Option<Self::Item>, Self::Error> {
        self.channel_reciever.poll()
    }
}

impl Sink for Link {
    type SinkItem = BytesUtf8View;
    type SinkError = mpsc::error::SendError;

    fn start_send(&mut self, item: Self::SinkItem) -> StartSend<Self::SinkItem, Self::SinkError> {
        let mut guard = match self.state.links().try_lock() {
            Ok(g) => g,
            Err(_) => return Ok(AsyncSink::NotReady(item)),
        };

        for (_, channel_sender) in guard.iter_mut() {
            if channel_sender.poll_ready()?.is_not_ready() {
                return Ok(AsyncSink::NotReady(item));
            };
        }

        for (addr, channel_sender) in guard.iter_mut() {
            if *addr != self.addr {
                channel_sender.start_send(item.clone())?;
            }
        }

        Ok(AsyncSink::Ready)
    }

    fn poll_complete(&mut self) -> Poll<(), Self::SinkError> {
        Ok( Async::Ready(()) )
    }

    fn close(&mut self) -> Poll<(), Self::SinkError> {
        Ok( Async::Ready(()) )
    }
}

impl Drop for Link {
    fn drop(&mut self) {
        let addr = self.addr;

        self.state.links()
            .with(move |mut guard| {
                guard.remove(&addr);
                Ok(()) as Result<(), ()>
            })
            .expect("failed to spawn 'remove' task");
    }
}
