use super::link::Link;

use crate::lines_codec::BytesUtf8View;

use tokio::{ prelude::*, sync::mpsc };

use std::{collections::HashMap, net::SocketAddr, sync::Arc};

use futures_locks as lock;



pub type ChannelReciever = mpsc::Receiver<BytesUtf8View>;
pub type ChannelSender = mpsc::Sender<BytesUtf8View>;

type Links = Arc<lock::Mutex<HashMap<SocketAddr, ChannelSender>>>;

#[derive(Clone, Debug)]
pub struct State {
    links: Links,
}

impl State {
    pub fn new() -> Self {
        let links = Arc::new(lock::Mutex::new(HashMap::new()));
        Self { links }
    }

    pub fn create_link(&self, addr: SocketAddr) -> LinkCreate {
        LinkCreate::new(addr, self.clone())
    }

    pub fn links(&self) -> Links {
        self.links.clone()
    }
}


pub struct LinkCreate {
    addr: SocketAddr,
    state: State,
}

impl LinkCreate {
    fn new(addr: SocketAddr, state: State) -> Self {
        Self { addr, state }
    }
}

impl Future for LinkCreate {
    type Item = Link;
    type Error = ();

    fn poll(&mut self) -> Poll<Self::Item, Self::Error> {
        let mut guard = self.state.links().try_lock()?;
        let (channel_sender, channel_reciever) = mpsc::channel(32);
        guard.insert(self.addr, channel_sender);

        let link = Link::new(self.addr, self.state.clone(), channel_reciever);
        Ok(Async::Ready(link))
    }
}
